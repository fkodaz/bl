<?php
include("fb.php");
date_default_timezone_set("Europe/London");

$arrContextOptions=array(
    "ssl"=>array(
        "verify_peer"=>false,
        "verify_peer_name"=>false,
    ),
);

header('Content-Type: application/json');

$events=file_get_contents("https://graph.facebook.com/v2.11/balabamvenue/events?fields=cover%2Cname%2Cstart_time%2Cticket_uri%2Cdescription%2Cid%2Cevent_times&time_filter=upcoming&access_token=$accessToken", false, stream_context_create($arrContextOptions));

$eventsData=json_decode($events,true);

if(!empty($eventsData))
{
  foreach($eventsData['data'] as $event){
    $ev=$event;
    if (!empty($ev['event_times'])){
      usort($ev['event_times'], "cmp");
      
      if(!empty($ev['event_times']))
      {
       $a=closest($ev['event_times'],date('c'));
       $ev['start_time']=date('c',$a);
      }
    }
            $eventsFull['data'][]=$ev;
  }
}

usort($eventsFull['data'], "cmp");

print_r(json_encode($eventsFull));

function cmp($a, $b) {
    if ($a['start_time'] == $b['start_time']) return 0;
    return (strtotime($a['start_time']) < strtotime($b['start_time']))? -1 : 1;
}

function closest($dates, $findate)
{
    $newDates = array();

        foreach($dates as $date)
        {
            $newDates[] = strtotime($date['start_time']);
        }


        sort($newDates);
        foreach ($newDates as $a)
        {
            if ($a >= strtotime($findate))
                return $a;
        }
   
        return end($newDates);
    }
?>



