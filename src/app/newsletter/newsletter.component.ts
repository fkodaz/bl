import {Component, OnInit} from '@angular/core';
import {AppService} from '../app.service';
import {FormGroup, FormControl, Validators} from '@angular/forms';

@Component({
    selector: 'app-newsletter',
    templateUrl: './newsletter.component.html',
    styleUrls: ['./newsletter.component.scss']
})
export class NewsletterComponent implements OnInit {

    newsletterForm: FormGroup;
    showSuccess = false;

    constructor(private appService: AppService) {
        this.newsletterForm = new FormGroup({
            name: new FormControl('', [Validators.required]),
            email: new FormControl('', Validators.required),
            checkbox: new FormControl(true),
        });
    }

    ngOnInit() {
    }

    send(data) {
        this.validateAllFields(this.newsletterForm);
        if (data) {
            this.appService.newsletter(this.newsletterForm.value).subscribe((d: any) => {
                this.showSuccess = true;
                this.newsletterForm = new FormGroup({
                    name: new FormControl('', [Validators.required]),
                    email: new FormControl('', Validators.required),
                    checkbox: new FormControl(true),
                });
            });
        }
    }

    close() {
        this.appService.showNewsletter = false;
    }


    validateAllFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({onlySelf: true});
            } else if (control instanceof FormGroup) {
                this.validateAllFields(control);
            }
        });
    }
}
