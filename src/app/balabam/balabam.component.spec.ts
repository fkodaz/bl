import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BalabamComponent } from './balabam.component';

describe('BalabamComponent', () => {
  let component: BalabamComponent;
  let fixture: ComponentFixture<BalabamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BalabamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BalabamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
