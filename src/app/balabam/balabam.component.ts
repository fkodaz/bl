import {Component, OnInit, ViewChild} from '@angular/core';
import {routerTransition} from '../animations/fade.animation';
import {MalihuScrollbarService} from 'ngx-malihu-scrollbar';

@Component({
    selector: 'app-balabam',
    templateUrl: './balabam.component.html',
    styleUrls: ['./balabam.component.scss'],
    animations: [routerTransition()],
    host: {'[@routerTransition]': ''}
})

export class BalabamComponent implements OnInit {
    @ViewChild('player') player: any;
    played = true;
    timer : any;
    time=0;

    constructor(private mScrollbarService: MalihuScrollbarService) {
    }

    ngOnInit() {
        if (window.outerWidth > 900) {
            this.mScrollbarService.initScrollbar('#scroll', {axis: 'y', theme: 'light'});
        }
    }

    toggleVideo() {
        this.player.nativeElement.play();

        if (this.played) {
            this.player.nativeElement.pause();
        } else {
            this.player.nativeElement.play();
        }
        this.played = !this.played;

    }

}
