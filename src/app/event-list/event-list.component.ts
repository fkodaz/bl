import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {routerTransition} from '../animations/fade.animation';
import {AppService} from '../app.service';
import * as _ from 'lodash';

@Component({
    selector: 'app-event-list',
    templateUrl: './event-list.component.html',
    styleUrls: ['./event-list.component.scss'],
    animations: [routerTransition()],
    host: {'[@routerTransition]': ''}
})
export class EventListComponent implements OnInit {

    @ViewChild('messagesContainer') messagesContainer: ElementRef;
    @ViewChild('right') right;
    @ViewChild('left') left;
    events: any;
    carouselOne: any;
    currentSlide: any = '01';
    prevAvailable:boolean=false;
    nextAvailable:boolean=true;

    constructor(private appService: AppService) {
    }

    ngOnInit() {

        this.carouselOne = {
            grid: {xs: 1, sm: 1, md: 5, lg: 5, all: 0},
            slide: 4,
            speed: 400,
            interval: 7000,
            point: {
                visible: true
            },
            load: 2,
            touch: true,
            loop: true,
            custom: 'banner'
        }
        this.appService.eventsData.subscribe(data => {
            this.events = data;
        });
        let th=this;
    }

    next() {
        this.right.nativeElement.click();
    }

    prev() {
        this.left.nativeElement.click();
    }

    scrollTo(element, to, duration) {
    }

    interest(event) {
        this.appService.activeEvent = event;
        this.appService.showInterest = true;
        this.appService.eventClickDetail = false;
    }


    newsletter() {
        this.appService.showNewsletter = true;
    }

    test(e) {
        let a = '';

        if (((e.currentSlide / 4) + 1) < 10) {
            a = '0' + Math.round((e.currentSlide / 4) + 1).toString();
        }
        this.currentSlide = a;

        if(e.currentSlide==0){
            this.prevAvailable=false;
        }
        else
        {
            this.prevAvailable=true;
        }

        if(e.itemLength==e.currentSlide+5){
            this.nextAvailable=false;
        }
        else
        {
            this.nextAvailable=true;
        }

    }
}
