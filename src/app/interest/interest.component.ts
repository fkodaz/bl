import { Component, OnInit } from '@angular/core';
import {AppService} from '../app.service';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import { ShareButtons } from '@ngx-share/core';
import { FacebookService, UIParams, UIResponse, InitParams } from 'ngx-facebook';


@Component({
  selector: 'app-interest',
  templateUrl: './interest.component.html',
  styleUrls: ['./interest.component.scss']
})
export class InterestComponent implements OnInit {

    interestForm: FormGroup;
    showSuccess = false;

    constructor(private fb: FacebookService,public share: ShareButtons,public appService: AppService) {
        this.interestForm = new FormGroup({
            name: new FormControl('', [Validators.required]),
            email: new FormControl('', Validators.required),
            checkbox: new FormControl(true),
            eventName: new FormControl(''),
            date: new FormControl('')
        });

    }

    ngOnInit() {
    }

    shareFb(url: string) {


        window.open('https://www.facebook.com/dialog/send?app_id=1847575362135458&link='+url+'&display=popup&redirect_uri=https://www.facebook.com/dialog/return/close#_=_', 'sharer', 'toolbar=0,status=0,width=500px,height=500px');

    }

    send(data) {
        this.validateAllFields(this.interestForm);
        if (data) {
            this.appService.interest(this.interestForm.value).subscribe((d: any) => {
                this.showSuccess = true;
                this.interestForm = new FormGroup({
                    name: new FormControl('', [Validators.required]),
                    email: new FormControl('', Validators.required),
                    checkbox: new FormControl(true),
                    eventName: new FormControl(this.appService.activeEvent.name),
                    date: new FormControl(this.appService.activeEvent.start_time)
                });
            });
        }
    }

    close() {
        this.appService.showInterest = false;
        return false;
    }

    validateAllFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({onlySelf: true});
            } else if (control instanceof FormGroup) {
                this.validateAllFields(control);
            }
        });
    }

    enc(data){
        return encodeURIComponent(data);
    }

}
