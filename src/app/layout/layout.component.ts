import { Component, OnInit } from '@angular/core';
import {AppService} from '../app.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  menuOpened = false;

  constructor(public appService: AppService) {
  }

  ngOnInit() {
  }

  openMenu() {
    this.menuOpened = !this.menuOpened;
  }

  closeMenu() {
    this.menuOpened = false;
  }
  newsletter(){
    this.appService.showNewsletter= true;
    return false;
  }
}
