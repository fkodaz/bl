import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FacebookModule, FacebookService } from 'ngx-facebook';
import { LayoutComponent } from './layout/layout.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { EventListComponent } from './event-list/event-list.component';
import { EventDetailComponent } from './event-detail/event-detail.component';
import { ReviewsComponent } from './reviews/reviews.component';
import { ContactComponent } from './contact/contact.component';

import { MalihuScrollbarModule } from 'ngx-malihu-scrollbar';
import { AgmCoreModule } from '@agm/core';
import { InterestComponent } from './interest/interest.component';
import { AppService } from './app.service';
import { BalabamComponent } from './balabam/balabam.component';
import { FoodComponent } from './food/food.component';
import { NewsletterComponent } from './newsletter/newsletter.component';
import { HttpModule } from '@angular/http';
import { NgxCarouselModule } from 'ngx-carousel';
import { BrPipe } from './br.pipe';
import 'hammerjs';
import { ShareButtonsModule } from '@ngx-share/buttons';

import { ReactiveFormsModule, FormControl, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
@NgModule({
  declarations: [
    LayoutComponent,
    EventListComponent,
    EventDetailComponent,
    ReviewsComponent,
    ContactComponent,
    InterestComponent,
    BalabamComponent,
    FoodComponent,
    NewsletterComponent,
    BrPipe
  ],
  imports: [
    BrowserModule,
      HttpModule,
      AppRoutingModule,
      HttpClientModule,
      BrowserAnimationsModule,
      MalihuScrollbarModule.forRoot(),
      AgmCoreModule.forRoot({
          apiKey: 'AIzaSyChidWlfkSiScpB5zDQIsxXu4wwm1V6LJc'
      }),
      NgxCarouselModule,
      FormsModule,
      ReactiveFormsModule,
      HttpClientModule,
      ShareButtonsModule.forRoot(),
      FacebookModule
  ],
  providers: [AppService, FacebookService],
  bootstrap: [LayoutComponent]
})
export class AppModule { }
