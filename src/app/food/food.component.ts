import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../animations/fade.animation';

@Component({
  selector: 'app-food',
  templateUrl: './food.component.html',
  styleUrls: ['./food.component.scss'],
  animations: [routerTransition()],
    host: {'[@routerTransition]': ''}
})
export class FoodComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}