import {Injectable} from '@angular/core';
import {Http, Response, RequestOptions, Headers} from '@angular/http';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import * as _ from 'lodash';

@Injectable()
export class AppService {

  showInterest = false;
  showNewsletter = false;
  activeEvent: any;
  eventsData = new BehaviorSubject('');
  reviewsData = new BehaviorSubject({});
  eventClickDetail = false;

  constructor(private http: Http) {
    this.events();
    this.reviews();
  }


  reviews() {
    return this.http.get('https://www.balabam.co.uk/service/reviews.json').subscribe(data => {
      const reviewsData = data.json();
      this.reviewsData.next(reviewsData);
    });
  }


  events() {
    return this.http.get('https://www.balabam.co.uk/service/events.php').subscribe(data => {
      const eventsData = data.json().data;
      this.eventsData.next(eventsData);
    });
  }

    newsletter(data) {
        return this.http.post('https://www.balabam.co.uk/service/newsletter.php', JSON.stringify(data));
    }

    interest(data) {
        return this.http.post('https://www.balabam.co.uk/service/interest.php', JSON.stringify(data));
    }
}

