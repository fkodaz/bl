import {Component, OnInit} from '@angular/core';
import {routerTransition} from '../animations/fade.animation';
import {MalihuScrollbarService} from 'ngx-malihu-scrollbar';
import {ActivatedRoute} from '@angular/router';
import {AppService} from '../app.service';
import * as _ from 'lodash';
import { Meta , Title } from '@angular/platform-browser';

@Component({
    selector: 'app-event-detail',
    templateUrl: './event-detail.component.html',
    styleUrls: ['./event-detail.component.scss'],
    animations: [routerTransition()],
    host: {'[@routerTransition]': ''}
})
export class EventDetailComponent implements OnInit {
    eventId: number;
    eventDetail: any;
    events: any;
    prevEvent: any;
    nextEvent: any;

    constructor(private title: Title, private meta: Meta, private mScrollbarService: MalihuScrollbarService, route: ActivatedRoute, private appService: AppService) {
        this.eventId = route.snapshot.params['id'];
    }

    ngOnInit() {



        if (window.outerWidth > 900) {
            this.mScrollbarService.initScrollbar('#scroll', {axis: 'y', theme: 'light'});
        }

        this.appService.eventsData.subscribe(data => {
            this.events = data;

            if (data !== '') {
                this.changeDetail(this.eventId);
            }
        });

    }

    changeDetail(id) {
        this.prevEvent = null;
        this.nextEvent = null;
        const index = _.findIndex(this.events, {'id': id});

        this.eventDetail = this.events[index];
        this.appService.activeEvent = this.eventDetail;

        this.meta.addTag({ property: 'og:title', content: this.eventDetail.name });
        this.meta.addTag({ property: 'og:image', content: this.eventDetail.cover.source });
        this.meta.addTag({ property: 'og:description', content: this.eventDetail.description });
        this.title.setTitle(this.eventDetail.name);


        if (index > 0) {
            this.prevEvent = this.events[index - 1];
        }

        if (this.events[index + 1]) {
            this.nextEvent = this.events[index + 1];
        }
    }

    interest() {
        this.appService.showInterest = true;
        this.appService.eventClickDetail = true;
        return false;
    }


}
