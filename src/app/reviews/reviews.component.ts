import {Component, OnInit} from '@angular/core';

import {routerTransition} from '../animations/fade.animation';
import {MalihuScrollbarService} from 'ngx-malihu-scrollbar';
import {AppService} from '../app.service';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.scss'],
  animations: [routerTransition()],
  host: {'[@routerTransition]': ''}
})
export class ReviewsComponent implements OnInit {
  reviews: any;

  constructor(private appService: AppService, private mScrollbarService: MalihuScrollbarService) {

  }

  ngOnInit() {
    this.appService.reviewsData.subscribe(data => {
      this.reviews = data;
    });
    if (window.outerWidth > 900) {
      this.mScrollbarService.initScrollbar('#scroll', {axis: 'y', theme: 'light'});
    }
  }

}
