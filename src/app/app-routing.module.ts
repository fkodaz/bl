import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EventListComponent } from './event-list/event-list.component';
import { EventDetailComponent } from './event-detail/event-detail.component';
import { ReviewsComponent } from './reviews/reviews.component';
import { ContactComponent } from './contact/contact.component';
import { FoodComponent } from './food/food.component';
import { BalabamComponent } from './balabam/balabam.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'events',
        pathMatch: 'full'
    },
    {
        path: 'events',
        component: EventListComponent
    },
    {
        path: 'event-detail/:id',
        component: EventDetailComponent
    },
    {
        path: 'reviews',
        component: ReviewsComponent
    },
    {
        path: 'contact',
        component: ContactComponent
    },
    {
        path: 'balabam',
        component: BalabamComponent
    },
    {
        path: 'food',
        component: FoodComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    providers: []
})

export class AppRoutingModule { }